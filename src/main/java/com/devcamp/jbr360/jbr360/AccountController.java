package com.devcamp.jbr360.jbr360;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount() {
        ArrayList<Account> listAcc = new ArrayList<Account>();

        Account account1 = new Account("TK01", "QuanNM");
        Account account2 = new Account("TK02", "BoiHB", 800);
        Account account3 = new Account("TK03", "ThuNHM", 400);

        listAcc.add(account1);
        listAcc.add(account2);
        listAcc.add(account3);

        return listAcc;
    }
    
    // public static void main(String[] args) {
    //     Account account1 = new Account("TK01", "QuanNM");
    //     Account account2 = new Account("TK02", "BoiHB", 800);
    //     System.out.println(account1 + "," + account2);
    //     System.out.println("---------------------");
    //     account1.credit(200);
    //     account2.debit(150);
    //     System.out.println(account1 + "," + account2);
    //     System.out.println("---------------------");
    //     account2.transferTo(account1, 200);
    //     System.out.println(account1 + "," + account2);
    // }
}
