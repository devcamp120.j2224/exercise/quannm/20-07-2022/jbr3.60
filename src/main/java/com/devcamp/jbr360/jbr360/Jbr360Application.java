package com.devcamp.jbr360.jbr360;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr360Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr360Application.class, args);
	}

}
